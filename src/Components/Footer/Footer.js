import React from "react";
import { Desktop, Mobile, Tablet } from "../../HOC/Responsive";
import FooterDesktop from "./FooterDesktop";
import FooterMobie from "./FooterMobie";
import FooterTablet from "./FooterTablet";

export default function Footer() {
  return (
    <div>
      <Desktop>
        <FooterDesktop />
      </Desktop>
      <Tablet>
        <FooterTablet />
      </Tablet>
      <Mobile>
        <FooterMobie />
      </Mobile>
    </div>
  );
}
